import Route from '@ember/routing/route';

export default Route.extend({
  model(params) {
    let events = this.modelFor('events');
    return events.find((event) => {
      return event.id === params.id;
    });
  }
});
