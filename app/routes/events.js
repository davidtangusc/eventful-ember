import Route from '@ember/routing/route';
import $ from 'jquery';

export default Route.extend({
  queryParams: {
    keywords: {
      refreshModel: true,
      as: 'keywords'
    }
  },
  model(params) {
    return $.getJSON(`https://api-eventful.herokuapp.com/api/events?keywords=${params.keywords}`);
  }
});
