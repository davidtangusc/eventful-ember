import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    searchForEvents() {
      this.transitionToRoute('events', {
        queryParams: {
          keywords: this.get('keywords')
        }
      });
    }
  }
});
